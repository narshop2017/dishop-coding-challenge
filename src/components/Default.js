import React, { Component } from 'react'

export class Default extends Component {
    render() {
        return (
            <div>
                <div className="pagetitle">
                    <div className="container">
                        <div className="pagetitle-wrapper d-flex justify-content-between align-items-center">
                            <div className="pagetitle-right">
                                Error
                            </div>

                        </div>

                    </div>
                </div>
                <div className="container">
                    <div className="row py-5">
                        Oops, something went wrong...

                    </div>

                </div>
            </div>

        )
    }
}

export default Default
