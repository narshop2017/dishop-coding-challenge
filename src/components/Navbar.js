import React, { Component } from 'react'
import { Link } from 'react-router-dom';
export class Navbar extends Component {
    render() {
        return (
            <div className="navbar">
                <div className="container">
                    <div className="navbar-wrapper d-flex justify-content-between align-items-center">
                        <div className="navbar-right">
                            <Link to="/">DEMO Streaming</Link>

                        </div>
                        <div className="navbar-left">
                            <Link to="/login">Log in</Link>
                            <Link to="/signup" className="signup">Start your free trial</Link>
                        </div>
                    </div>

                </div>
            </div>

        )
    }
}

export default Navbar
