import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import Card from "./Card";
import { AppConsumer } from "../context";
export class Films extends Component {
    render() {
        return (
            <AppConsumer>
                {value => {
                    const { errorMessage, isLoading, sample } = value;
                    const tmpsample = sample.filter(i => i.programType === "movie" && i.releaseYear >= 2010).slice(0, 21).sort((a, b) => a.title.localeCompare(b.title));
                    return (
                        <div>
                            <div className="pagetitle">
                                <div className="container">
                                    <div className="pagetitle-wrapper d-flex justify-content-between align-items-center">
                                        <div className="pagetitle-right">
                                            Popular Films
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div className="container">
                                <div className="row py-5">
                                    {
                                        errorMessage ? <div>Oops, something went wrong...</div> :

                                            isLoading ? <div>Loading...</div> :


                                                tmpsample.map(item => {
                                                    return (
                                                        <Link to={item.title} key={item.id}>
                                                            <Card item={item} />
                                                        </Link>
                                                    )
                                                })


                                    }


                                </div>

                            </div>
                        </div>
                    );
                }}
            </AppConsumer>

        )
    }
}

export default Films
