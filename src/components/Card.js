import React, { Component } from 'react'

export class Card extends Component {
    render() {
        console.log(this.props.item)
        return (
            <div className="card-wrapper">
                <img alt="" src={this.props.item.images["Poster Art"].url} />
                <p>{this.props.item.title}</p>
            </div>
        )
    }
}

export default Card
