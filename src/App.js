import React, { Component } from "react";
import './App.css';
import { Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Accueil from "./components/Accueil";
import Films from "./components/Films";
import Series from "./components/Series";
import Default from "./components/Default";
import Navbar from "./components/Navbar";
class App extends Component {
  render() {
    return (
      <>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Accueil} />
          <Route path="/films" component={Films} />
          <Route path="/series" component={Series} />
          <Route component={Default} />
        </Switch>
      </>
    );
  }
}

export default App;
